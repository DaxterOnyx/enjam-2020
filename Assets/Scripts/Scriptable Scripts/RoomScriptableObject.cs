﻿using System.Diagnostics;
using System.Drawing;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

[CreateAssetMenu(fileName = "Room", menuName = "ScriptableObjects/Room", order = 1)]
public class RoomScriptableObject : ScriptableObject
{
	public Vector2Int Size = new Vector2Int(10, 10);
	[Min(0)]
	[Tooltip("The position of the door (point d'entrée) on the walls, min = 0, max = (Size.x+Size.Y)*2.")]
	public int DoorPos = 0;
	[Min(0)]
	[Tooltip("The position of the window (point de sortie) on the walls, min = 0, max = (Size.x+Size.Y)*2.")]
	public int WindowDoor = 0;
}
