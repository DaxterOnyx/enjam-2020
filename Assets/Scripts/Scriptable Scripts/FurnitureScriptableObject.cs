﻿using UnityEngine;

[CreateAssetMenu(fileName = "Furniture", menuName = "ScriptableObjects/Furniture", order = 1)]
public class FurnitureScriptableObject : ScriptableObject
{
//	public Vector2Int Dimension = new Vector2Int(1, 1);
	public float Height = 1;
	public Sprite Texture;
}
