﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Window : MonoBehaviour
{
    public float TimeAnimation = 1;
    public Vector2 voidPosition;
    public int ScoreBonus = 1000;
    private Collider2D Player;
    public GameObject Crash;
    [SerializeField] private GameObject emoji;
    public GameObject Intact;
    public GameObject Breaking;
    public GameObject Broken;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player = collision;
            StartAnimPlayer();
            StartAnimWindow();
            //AudioListener
            GetComponent<AudioSource>().Play();
            emoji.GetComponent<scr_emoji>().Fall();
            ScoreManager.Fall(Player.transform.position);
        }
    }

    public void Reset()
    {
        Intact.SetActive(true);
        Breaking.SetActive(false);
        Broken.SetActive(false);
    }

    private void StartAnimWindow()
    {
        Intact.SetActive(false);
        Breaking.SetActive(true);
        Invoke("EndAnimWindow", 0.3f);
    }

    private void EndAnimWindow()
    {
        Breaking.SetActive(false);
        Broken.SetActive(true);
    }

    private void StartAnimPlayer()
    {
        Player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        Player.transform.DOMove(voidPosition + (Vector2)transform.position, TimeAnimation / 3f);
        Player.GetComponent<Collider2D>().enabled = false;
        Player.GetComponent<CharacterMovement>().enabled = false;
        //Player.GetComponent<CharacterController>().enabled = false;
        Player.GetComponent<VelocityPlayer>().enabled = false;
        Invoke("Fall", TimeAnimation / 3f);
        Invoke("EmitParticule", 2 * TimeAnimation / 3f);
        Invoke("EndGame", TimeAnimation);
    }

    void EmitParticule()
    {
        GameObject go = Instantiate(Crash, Player.transform.position, Quaternion.identity);
        Destroy(go, 1);
    }

    void Fall()
    {
        Player.transform.DOScale(0, TimeAnimation / 3f);
    }

    void EndGame()
    {
        UIScript.gameOver = true;
    }
}
