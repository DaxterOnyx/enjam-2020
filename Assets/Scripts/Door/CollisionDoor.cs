﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class CollisionDoor : MonoBehaviour
{
    public float bumperForce = 1;
    public bool Horizontal;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Vector2 angle = collision.collider.attachedRigidbody.velocity.normalized;
            Vector2 result;
            if (Horizontal)
                result = new Vector2(angle.x, -angle.y);
            else
                result = new Vector2(-angle.x, angle.y);

            collision.gameObject.GetComponent<CharacterMovement>().enabled = false;
            collision.gameObject.GetComponent<VelocityPlayer>().enabled = true;
            collision.gameObject.GetComponent<VelocityPlayer>().m_velocity = result * bumperForce;


            //Score
            if (bumperForce == 5)
            {
                ScoreManager.Corner(collision.transform.position);
            }
            else
            {
                ScoreManager.Plane(collision.transform.position);
            }

        }
    }
}
