﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class Door : MonoBehaviour
{
    
    public GameObject open;
    public GameObject close;
    public AudioSource AS;
    public GameObject dash;

    public void Update()
    {
        if (!dash.activeSelf)
            Close();
    }

    public void Close()
    {
        open.SetActive(false);
        close.SetActive(true);
    }

    public void Open()
    {
        open.SetActive(true);
        close.SetActive(false);
    }
}
