﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class CollisionReaction : MonoBehaviour
{
    public float bumperForce = 1;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Vector2 angle = collision.collider.attachedRigidbody.velocity.normalized;
            float force = collision.collider.attachedRigidbody.velocity.magnitude;
            Vector2 normalSurface = collision.GetContact(0).normal;

            Debug.DrawLine(this.transform.position, new Vector3(normalSurface.x, normalSurface.y, 0));

            Vector2 result = Vector2.Reflect(angle, normalSurface);
            collision.gameObject.GetComponent<CharacterMovement>().enabled = false;
            collision.gameObject.GetComponent<VelocityPlayer>().enabled = true;
            collision.gameObject.GetComponent<VelocityPlayer>().m_velocity = result * bumperForce;

            //collision.gameObject.GetComponent<CharacterRandomSound>().PlayRandom();
            Vector2 position = new Vector2(collision.transform.position.x, collision.transform.position.y);

            //Score
            if (bumperForce == 5)
            {
                ScoreManager.Corner(position);
            }
            else
            {
                ScoreManager.Plane(position);
            }

        }
    }
}
