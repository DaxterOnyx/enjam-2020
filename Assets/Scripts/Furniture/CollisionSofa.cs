﻿using System.Collections;

using UnityEngine;

public class CollisionSofa : MonoBehaviour
{
    public float timeBeforeLeave = 1;
    public GameObject player, spritePlayerOnSofa;
    public Vector3 positionPlayer;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine("PlayerSitDown");
        }
    }

    IEnumerator PlayerSitDown()
    {
        player.SetActive(false);
        spritePlayerOnSofa.SetActive(true);
        yield return new WaitForSeconds(timeBeforeLeave);

        player.GetComponent<CharacterMovement>().enabled = true;
        player.GetComponent<VelocityPlayer>().enabled = false;

        player.transform.position = this.transform.position + positionPlayer;
        player.transform.rotation = this.transform.rotation;
        player.SetActive(true);
        spritePlayerOnSofa.SetActive(false);
    }

}
