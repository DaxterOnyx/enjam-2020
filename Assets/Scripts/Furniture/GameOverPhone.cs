﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverPhone : MonoBehaviour
{
    //public GameObject Fond;
    private GameObject target;

    private void Start()
    {
        target = transform.Find("Target").gameObject;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            GetComponent<AudioSource>().Play();
            GameObject player = collision.gameObject;
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            player.GetComponent<Rigidbody2D>().angularVelocity = 0;
            player.GetComponent<Collider2D>().enabled = false;
            player.GetComponent<VelocityPlayer>().enabled = false;
            player.GetComponent<CharacterMovement>().enabled = false;
            player.transform.position = target.transform.position;
            player.transform.rotation = transform.rotation;
            GameObject.Find("Emoji").GetComponent<scr_emoji>().Sad();
            transform.Find("Notes").gameObject.SetActive(false);
            StartCoroutine("GameOver");
        }
    }


    IEnumerator GameOver()
    {

        //emoji.GetComponent<scr_emoji>().Calm();
       // Fond.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        UIScript.gameOver = true;
       // GameObject.Find("GameGestion").GetComponent<GameGestion>().Reset();
        //SceneManager.LoadScene("GameOverMenu");
    }
}
