﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class CollisionDestroyableItem : MonoBehaviour
{
    public float addForce = 6;
    public SpriteRenderer sprite;
    public Sprite destroyedFurniture, furniture;
    public GameObject highLight;
    public Collider2D colliderToDesactivate;
    public AudioSource AS;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<CharacterMovement>().enabled = false;
            collision.gameObject.GetComponent<VelocityPlayer>().enabled = true;
            collision.gameObject.GetComponent<VelocityPlayer>().m_velocity = collision.attachedRigidbody.velocity.normalized * addForce;
            colliderToDesactivate.enabled = false;
            highLight.SetActive(false);
            sprite.sprite = destroyedFurniture;
            sprite.sortingLayerName = "Default";
            sprite.sortingOrder = 10;
            ScoreManager.Break(collision.transform.position);
            AS.Play();
        }
    }

    public void Restart()
    {
        colliderToDesactivate.enabled = true;
        highLight.SetActive(true);
        sprite.sprite = furniture;
        sprite.sortingLayerName = "Furniture";
        sprite.sortingOrder = 0;

    }
}
