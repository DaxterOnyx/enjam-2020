﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureControl : MonoBehaviour
{
	[SerializeField] public static bool canBePick, m_phasePlacement;
	//public FurnitureScriptableObject Data;
	public Collider2D m_collider;
	public SpriteRenderer SpriteRenderer, highLightRenderer;
	public bool isDragged, canBeDrop;
	public GameObject colliders;
	private Camera m_camera;
    public Texture2D CursorOpenSprite;
    public Texture2D CursorCloseSprite;

    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    public void Reset()
    {
        m_phasePlacement = true;
        canBePick = true;
        isDragged = false;
        canBeDrop = true;
        //Collider.size = Data.Dimension;
        //SpriteRenderer.sprite = Data.Texture;
        m_camera = Camera.main;
		colliders.SetActive(false);
	}

    private void Update()
    {
		if (UIScript.canPlay)
		{
			if (m_phasePlacement)
			{
				if (isDragged)
				{
					float rotation = Input.GetAxis("Mouse ScrollWheel");
					this.transform.Rotate(new Vector3(0, 0, rotation * 20));
					Vector2 mousePosition = GetMouseWorldPos();
					transform.position = mousePosition;

				}
			}
			else
			{
				colliders.SetActive(true);
			}
		}
	}

    private void OnMouseEnter()
	{
		if(m_phasePlacement && UIScript.canPlay)
		highLightRenderer.color = Color.white;
	}

	private void OnMouseExit()
	{
		if (m_phasePlacement && UIScript.canPlay)
			highLightRenderer.color = new Color(0.2f,0.2f,0.2f,1);
	}

	private void OnMouseDown()
	{
		if (m_phasePlacement && UIScript.canPlay)
		{
			if (isDragged && canBeDrop)
			{
				isDragged = false;
				canBePick = true;
				SpriteRenderer.sortingOrder = 0;
				highLightRenderer.sortingOrder = 1;

				Cursor.SetCursor(CursorOpenSprite, new Vector2(CursorOpenSprite.width / 2, CursorOpenSprite.height / 2),CursorMode.Auto);
			}
			else if (!isDragged && canBePick)
			{
				isDragged = true;
				canBePick = false;

				SpriteRenderer.sortingOrder = 10;
				highLightRenderer.sortingOrder = 11;
				Cursor.SetCursor(CursorCloseSprite, new Vector2(CursorCloseSprite.width / 2, CursorCloseSprite.height / 2),CursorMode.Auto);
			}
		}
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
		if (isDragged && UIScript.canPlay)
		{
			SpriteRenderer.color = Color.red;

			canBeDrop = false;
		}
    }

	private void OnTriggerExit2D(Collider2D collision)
    {
		if (isDragged && !m_collider.IsTouchingLayers() && UIScript.canPlay)
		{
			SpriteRenderer.color = Color.white;
			canBeDrop = true;
		}
	}

    private Vector2 GetMouseWorldPos()
	{
		Vector3 mousePos = Input.mousePosition;
		return m_camera.ScreenToWorldPoint(mousePos);
	}
}
