﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGestion : MonoBehaviour
{
    public GameObject player;
    public Animator catTree;
    [SerializeField] private GameObject emoji;
    public GameOverPhone GOP;

    public float timerMax = 30;
    public static float timer;
    public Vector2 velocityBegin;
    private bool alreadyvelocityBegin;

    private Vector2 PositionBegin;
    private Quaternion RotationBegin;
    private Vector3 ScaleBegin;
    public Door Door;

    [SerializeField] private GameObject ResetIcon;
    [SerializeField] private GameObject PlayIcon;

    // Start is called before the first frame update
    void Awake()
    {
        catTree.enabled = false;

        PositionBegin = player.transform.position;
        RotationBegin = player.transform.rotation;
        ScaleBegin = player.transform.localScale;
        alreadyvelocityBegin = false;
        FurnitureControl.m_phasePlacement = true;
        timer = timerMax;
    }

    // Update is called once per frame
    private void OnMouseDown()
    {
        if (FurnitureControl.m_phasePlacement)
        {
            catTree.enabled = true;
            catTree.SetTrigger("Go");
            FurnitureControl.m_phasePlacement = false;
            player.SetActive(true);
            emoji.SetActive(true);
            PlayIcon.SetActive(false);
            ResetIcon.SetActive(true);
        }

        else

        {

            Reset();


        }
    }



    public void Reset()

    {

        //var
        PlayIcon.SetActive(true);
        ResetIcon.SetActive(false);
        alreadyvelocityBegin = false;
                catTree.enabled = false;

        timer = timerMax;



        //Reset destrucible ojects

        CollisionDestroyableItem[] destructibles = FindObjectsOfType<CollisionDestroyableItem>();

        foreach (var item in destructibles)

        {

            item.Restart();

        }



        //Reset movable object

        FurnitureControl.m_phasePlacement = true;

        FurnitureControl[] furnitureControls = FindObjectsOfType<FurnitureControl>();

        foreach (var item in furnitureControls)

        {

            item.Reset();

        }



        //Reset Door

        Door.Open();



        //Reset character

        player.GetComponent<CharacterMovement>().enabled = false;
        player.GetComponent<Collider2D>().enabled = true;
        player.GetComponent<VelocityPlayer>().enabled = false;

        player.transform.position = PositionBegin;

        player.transform.rotation = RotationBegin;

        player.transform.localScale = ScaleBegin;

        player.SetActive(false);

        if(emoji.activeSelf)
            emoji.GetComponent<scr_emoji>().Calm();
        emoji.SetActive(false);



        //Reset Score

        ScoreManager.Score = 0;


        //ANIM DOTWEEN
        DOTween.Clear();
    }



    private void FixedUpdate()
    {
        if (!FurnitureControl.m_phasePlacement)
        {
            if (!alreadyvelocityBegin)
            {
                player.GetComponent<Rigidbody2D>().velocity = velocityBegin;
                alreadyvelocityBegin = true;
            }
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                GOP.StartCoroutine("GameOver");
            }
        }
    }
}
