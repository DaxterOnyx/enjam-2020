﻿using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator : MonoBehaviour
{
    public RoomScriptableObject data;

    public GameObject FloorPrefab;
    public GameObject WallPrefab;
    public Sprite WallUpSprite;
    public Sprite WallDownSprite;
    public Sprite WallLeftSprite;
    public Sprite WallRightSprite;
    public Sprite CornerUpRightSprite;
    public Sprite CornerUpLeftSprite;
    public Sprite CornerDownRightSprite;
    public Sprite CornerDownLeftSprite;
    public Sprite DoorUpSprite;
    public Sprite DoorDownSprite;
    public Sprite DoorRightSprite;
    public Sprite DoorLeftSprite;
    public Sprite WindowUpSprite;
    public Sprite WindowDownSprite;
    public Sprite WindowRightSprite;
    public Sprite WindowLeftSprite;

    public Sprite FloorSprite;
    public Sprite FloorUpSprite;
    public Sprite FloorDownSprite;
    public Sprite FloorLeftSprite;
    public Sprite FloorRightSprite;
    public Sprite FloorUpLeftSprite;
    public Sprite FloorDownRightSprite;
    public Sprite FloorDownLeftSprite;
    public Sprite FloorUpRightSprite;

    //private List<Wall> walls;
    //private int wallscount = 0;
    //private Wall Door;
    //private Wall Window;

    #region Initialisation
    private void Start()
    {
        //walls = new List<Wall>();
        //CreateSeed();
        CreateFloorAndWall();

    }

    private void CreateSeed()
    {
        throw new System.NotImplementedException();
    }

    private void CreateFloorAndWall()
    {
        for (int i = -1; i < data.Size.x + 1; i++)
        {
            for (int j = -1; j < data.Size.y + 1; j++)
            {
                if (i == -1)
                { // WALL LEFT
                    if (j == -1)
                    {
                        //CORNER DOWN LEFT
                        CreateWall(i, j, CornerDownLeftSprite, null, null, false);
                        CreateFloor(i, j, FloorDownLeftSprite);
                    }
                    else if (j == data.Size.y)
                    {
                        //CORNER UP LEFT
                        CreateWall(i, j, CornerUpLeftSprite, null, null, false);
                        CreateFloor(i, j, FloorUpLeftSprite);
                    }
                    else
                    {
                        //WALL LEFT
                        CreateWall(i, j, WallLeftSprite, DoorLeftSprite, WindowLeftSprite, true);
                        CreateFloor(i, j, FloorLeftSprite);
                    }
                }
                else if (i == data.Size.x)
                { //WALL RIGHT
                    if (j == -1)
                    {
                        //CORNER DOwN RIGHT
                        CreateWall(i, j, CornerDownRightSprite, null, null, false);
                        CreateFloor(i, j, FloorDownRightSprite);
                    }
                    else if (j == data.Size.y)
                    {
                        //CORNER UP RIGHT
                        CreateWall(i, j, CornerUpRightSprite, null, null, false);
                        CreateFloor(i, j, FloorUpRightSprite);
                    }
                    else
                    {
                        //WALL RIGHT
                        CreateWall(i, j, WallRightSprite, DoorRightSprite, WindowRightSprite, true);
                        CreateFloor(i, j, FloorRightSprite);
                    }
                }
                else if (j == -1)
                {
                    //WALL DOWN
                    CreateWall(i, j, WallDownSprite, DoorDownSprite, WindowDownSprite, true);
                    CreateFloor(i, j, FloorDownSprite);
                }
                else if (j == data.Size.y)
                {
                    //WALL UP
                    CreateWall(i, j, WallUpSprite, DoorUpSprite, WindowUpSprite, true);
                    CreateFloor(i, j, FloorUpSprite);
                }
                else
                {
                    //FLOOR
                    CreateFloor(i, j, FloorSprite);
                }
            }
        }
    }

    private void CreateFloor(int i, int j, Sprite sprite)
    {
        //TODO add Sprite
        GameObject floor = Instantiate(FloorPrefab, new Vector3(transform.position.x + i - data.Size.x / 2f, transform.position.y + j - data.Size.y / 2f, 0), new Quaternion(), transform);
        floor.GetComponent<SpriteRenderer>().sprite = sprite;
    }

    private void CreateWall(int i, int j, Sprite wallSprite, Sprite DoorSprite, Sprite WindowSprite, bool isWall)
    {
        GameObject go = Instantiate(WallPrefab, new Vector3(transform.position.x + i - data.Size.x / 2f, transform.position.y + j - data.Size.y / 2f, 0), new Quaternion(), transform);

        //Wall wall = go.GetComponent<Wall>();
        //walls.Add(wall);

        //Sprite sprite = wallSprite;
        //if (isWall)
        //{
        //    wallscount++;
        //    if (wallscount == data.DoorPos)
        //    {
        //        //Wall is door
        //        Door = wall;
        //        sprite = DoorSprite;
        //    }
        //    else if (wallscount == data.WindowDoor)
        //    {
        //        //Wall is window
        //        Window = wall;
        //        sprite = WindowSprite;
        //    }
        //}

        //go.GetComponent<SpriteRenderer>().sprite = sprite;
    }
    #endregion


}
