﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundGestion : MonoBehaviour
{
    public AudioClip[] audios;
    public AudioClip collisionClip;
    public AudioSource AS, AS2;
    public void PlayRandom()
    {
        int rand = Random.Range(0, audios.Length);
        AS.clip = audios[rand];
        AS.Play();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        AS2.clip = collisionClip;
        AS2.Play();
    }

}
