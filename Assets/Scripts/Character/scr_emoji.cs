﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class scr_emoji : MonoBehaviour
{

    public GameObject target;

    [SerializeField] private Vector3 offset;
    [SerializeField] private Sprite calmSprite;
    [SerializeField] private Sprite[] randomHurtSprite;
    [SerializeField] private Sprite fallSprite;
    [SerializeField] private Sprite sad;
    [SerializeField] private GameObject hands;
    [SerializeField] private GameObject face;
    private SpriteRenderer faceSpriteRenderer;
    

    private void Start()
    {
        faceSpriteRenderer = face.GetComponent<SpriteRenderer>();
        Calm();
    }

    // Update is called once per frame
    void Update()
    {
        JumpToPosition(target.transform.position);
    }

    void JumpToPosition(Vector3 targetPos)
    {
        transform.position = new Vector3(
            targetPos.x + offset.x,
            targetPos.y + offset.y,
            transform.position.z        
            );
    }

    public void Calm(){
        faceSpriteRenderer.sprite = calmSprite;
        DisplayHands(false);
    }

    public void DisplayHands(bool active)
    {
        hands.SetActive(active);
    }

    public void Hurt()
    {
        Sprite sprite;
        do {
            sprite = randomHurtSprite[Random.Range(0, randomHurtSprite.Length)];
        } while(sprite == faceSpriteRenderer.sprite);
        faceSpriteRenderer.sprite = sprite;
        target.GetComponent<SoundGestion>().PlayRandom();
    }

    public void Sad()
    {
        DisplayHands(false);
        faceSpriteRenderer.sprite = sad;
    }
    
    public void Fall()
    {
        faceSpriteRenderer.sprite = fallSprite;
        DisplayHands(true);
    }
}
