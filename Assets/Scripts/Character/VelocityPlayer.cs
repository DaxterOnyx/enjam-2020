﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityPlayer : MonoBehaviour
{
    public Vector2 m_velocity = new Vector2(0,-2);
    public float spinningForce = 2f;
    public float maxSpinningVelocity = 2f;
    public float speed = 2f;
    private scr_emoji emoji;

    // Start is called before the first frame update
    void Start()
    {
        emoji = GameObject.Find("Emoji").GetComponent<scr_emoji>();
        emoji.Hurt();
        emoji.DisplayHands(true);
    }
    void OnEnable() {
        if(emoji)
        {
            emoji.Hurt();
            emoji.DisplayHands(true);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + new Vector3(m_velocity.x, m_velocity.y, 0));

        m_velocity = m_velocity * 0.99f;
        this.GetComponent<Rigidbody2D>().velocity = m_velocity;

        if (m_velocity.magnitude > 4) this.GetComponent<Rigidbody2D>().AddTorque(spinningForce);
        if(this.GetComponent<Rigidbody2D>().angularVelocity > maxSpinningVelocity)
        {
            this.GetComponent<Rigidbody2D>().angularVelocity = maxSpinningVelocity;
        }
        this.GetComponent<Rigidbody2D>().angularVelocity *= 0.999f;

        if(m_velocity.magnitude < 1)
        {
            this.GetComponent<Rigidbody2D>().angularVelocity *= 0.6f;
            if (this.GetComponent<Rigidbody2D>().angularVelocity <= 0.8f)
            {
                StartCoroutine("WaitAndGo");
            }
        }
    }

    IEnumerator WaitAndGo()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        this.GetComponent<Rigidbody2D>().angularVelocity = 0;

        yield return new WaitForSeconds(.1f);
        emoji.DisplayHands(false);
        emoji.Sad();
        this.gameObject.GetComponent<CharacterMovement>().enabled = true;

        this.enabled = false;
    }
}
