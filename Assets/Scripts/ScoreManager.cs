﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static int Score = 0;
    public static int Highscore;
    public static GameObject scoreObject;

    public static Dictionary<int, Sprite> scoreSprites;

    public static int AddScore(int score)
    {
        Score += score;
        SaveScore();
        return Score;
    }

    public static int Corner(Vector2 position)
    {
        int value = 50;
        InstantiateScore(position, value);
        return AddScore(value);
    }

    public static int Plane(Vector2 position)
    {
        int value = 10;
        InstantiateScore(position, value);
        return AddScore(value);
    }

    public static int Break(Vector2 position)
    {
        int value = 200;
        InstantiateScore(position, value);
        return AddScore(value);
    }
    
    public static int Fall(Vector2 position)
    {
        int value = 1000;
        InstantiateScore(position, value);
        return AddScore(value);
    }

    public static int SaveScore()
    {
        if (Score > Highscore)
        {
            Highscore = Score;
            PlayerPrefs.SetInt("HighScore", Score);
        }
        return Score;
    }

    private void Start()
    {
        Highscore = PlayerPrefs.GetInt("Highscore");
        scoreObject = Resources.Load<GameObject>("Prefabs/ScoreObject");
        scoreSprites = new Dictionary<int, Sprite>();
        Sprite sprite;
        int[] scores = {10, 20, 50, 100, 200, 500, 1000};
        foreach(int i in scores)
        {
            sprite = Resources.Load<Sprite>("Sprites/Scores/" + i.ToString());
            if(!sprite)
                Debug.LogError("ScoreManager LOADING ERROR: resource \"Resources/Sprites/Scores/" + i.ToString() + "\" not found");
            scoreSprites.Add(i, sprite);
        }
    }

    private static void InstantiateScore(Vector2 position, int value)
    {
        var score = Object.Instantiate(scoreObject, position, Quaternion.identity);
        score.GetComponent<SpriteRenderer>().sprite = scoreSprites[value];
    }

    //TODO DISPLAY SCORE AND HIGHSCORE
}
