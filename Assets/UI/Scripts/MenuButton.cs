﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public int levelID;
    float currentTime = 0.2f;
    Vector2 newScale = new Vector2(1f, 1f);
    UIScript script;
    private void Start()
    {
        script = GameObject.Find("Canvas").GetComponent<UIScript>();
    }
    void Update()
    {
        if (currentTime < 0.2f)
        {
            currentTime += Time.deltaTime;
            this.transform.localScale = Vector2.Lerp(this.transform.localScale, newScale, currentTime * 5);
        }
    }
    public void In()
    {
        currentTime = 0;
        newScale = new Vector2(1.2f, 1.2f);
    }
    public void Out()
    {
        currentTime = 0;
        newScale = new Vector2(1f, 1f);
    }
    public void Click()
    {
        this.transform.localScale = Vector2.one;
    }
    public void SelectIn()
    {
        print(levelID);
        script.levelSelect = levelID;
        In();
    }
    public void SelectOut()
    {
       // script.levelSelect = 0;
        Out();
    }
    public void LevelSelect()
    {
        script.SelectLevel();
    }
}
