﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIScript : MonoBehaviour
{
    public GameObject levelButtonPrefab;
    public Sprite[] icons = new Sprite[4];
    public GameObject[] Levels;
    public AudioListener audioOn;
    TextMeshProUGUI begin;
    TextMeshProUGUI sound;
    TextMeshProUGUI score;
    TextMeshProUGUI status;
    TextMeshProUGUI best;
    TextMeshProUGUI timer;
    Transform statusScale;
    Transform levelScale;
    Transform levelsGrid;
    RectTransform UICanvas;
    CanvasGroup buttonCanvas;
    CanvasGroup scoreCanvas;
    CanvasGroup timerCanvas;
    CanvasGroup bestCanvas;
    CanvasGroup aboutCanvas;
    TextAnim statusAnim;
    TextAnim levelAnim;
    float openPos;
    float closedPos;
    float currentPos;
    float currentTime = 1;
    public int levelSelect;
    int levelNumber = 3;
    bool open = false;
    bool about = false;
    bool aboutActive = false;

    public static bool canPlay = true;
    public static bool gameOver = false;
    void Start()
    {
        gameOver = false;
        canPlay = false;
        // Assigner l'audiosource
        UICanvas = GameObject.Find("UICanvas").GetComponent<RectTransform>();
        begin = GameObject.Find("ButtonTitle").GetComponent<TextMeshProUGUI>();
        sound = GameObject.Find("ButtonTitle (1)").GetComponent<TextMeshProUGUI>();
        
        GameObject levelObject = GameObject.Find("LevelTitle");
        levelScale = levelObject.transform;
        levelAnim = levelObject.GetComponent<TextAnim>();
        levelAnim.active = false;
        GameObject bestObject = GameObject.Find("Best");
        bestCanvas = bestObject.GetComponent<CanvasGroup>();
        best = bestObject.GetComponent<TextMeshProUGUI>();
        GameObject scoreObject = GameObject.Find("Score");
        score = scoreObject.GetComponent<TextMeshProUGUI>();
        scoreCanvas = scoreObject.GetComponent<CanvasGroup>();
        GameObject timerObject = GameObject.Find("Timer");
        timerCanvas = timerObject.GetComponent<CanvasGroup>();
        timer = timerObject.GetComponent<TextMeshProUGUI>();

        GameObject statusObject = GameObject.Find("Status");
        statusAnim = statusObject.GetComponent<TextAnim>();
        statusAnim.active = false;
        status = statusObject.GetComponent<TextMeshProUGUI>();
        statusScale = statusObject.transform;
        aboutCanvas = GameObject.Find("About").GetComponent<CanvasGroup>();
        buttonCanvas = GameObject.Find("MenuButton").GetComponent<CanvasGroup>();
        levelsGrid = GameObject.Find("Levels").transform;
        openPos = 0f;
        closedPos = 8;
        OpenSelectionMenu();
    }
    void Update()
    {
        score.text = "Score : " + ScoreManager.Score;
        best.text = "Best : " + ScoreManager.Highscore;
        timer.text = "Time : " + (int)GameGestion.timer;
        if (currentTime < 1f)
        {
            currentTime += Time.deltaTime;
            UICanvas.position = new Vector2(Mathf.Lerp(UICanvas.position.x, currentPos, currentTime), 0);
        }
        if (gameOver)
        {
            gameOver = false;
            GameOver();
        }
    }
    public void OpenSelectionMenu()
    {
        canPlay = false;

        bestCanvas.alpha = 0;
        if (open)
        {
            StartCoroutine(Fade(true, buttonCanvas));
            StartCoroutine(Fade(true, scoreCanvas));
            StartCoroutine(Fade(true, timerCanvas));
            OpenMenu();
            StartCoroutine(HideText(statusScale, statusAnim));
        }
        StartCoroutine(DisplayText(levelScale, levelAnim));
        Levels[levelSelect].SetActive(false);

        int n = 0;
        for (int i = 0; i < levelNumber; i++)
        {
            GameObject newLevelButton = GameObject.Instantiate(levelButtonPrefab, levelsGrid);
            MenuButton script = newLevelButton.GetComponent<MenuButton>();
            n += 1;
            if (n > icons.Length - 1)
            {
                n = 0;
            }
            newLevelButton.GetComponent<Image>().sprite = icons[n];
            script.levelID = i;
        }
        StartCoroutine(DisplayLevels(false));
    }
    public void SelectLevel()
    {
        canPlay = false;

        // Ouvrir la scène correspondant à l'int selectLevel
        StartCoroutine(HideText(levelScale, levelAnim));
        StartCoroutine(DisplayLevels(true));
        StartCoroutine(Fade(false, buttonCanvas));
        StartCoroutine(Fade(false, scoreCanvas));
        StartCoroutine(Fade(false, timerCanvas));
        status.text = "Level " + (levelSelect + 1);
        Levels[levelSelect].SetActive(true);
        currentPos = openPos;
        currentTime = 0;
        open = true;
        StartCoroutine(DisplayText(statusScale, statusAnim));
    }
    public void Credits()
    {
        if (!aboutActive)
        {
            if (about)
            {
                aboutActive = true;
                StartCoroutine(Fade(true, aboutCanvas));
            }
            else
            {
                aboutCanvas.transform.GetChild(0).gameObject.SetActive(true);
                aboutActive = true;
                StartCoroutine(Fade(false, aboutCanvas));
            }
        }
    }
    public void GameOver()
    {
        canPlay = false;
        // à Appeler à la fin du niveau
        StartCoroutine(Fade(true, buttonCanvas));
        StartCoroutine(Fade(true, scoreCanvas));
        StartCoroutine(Fade(true, timerCanvas));
        StartCoroutine(Fade(false, bestCanvas));
        buttonCanvas.gameObject.SetActive(false);
        status.text = "Game Over\n\n" + score.text;
        currentPos = openPos;
        currentTime = 0;
        open = true;
        StartCoroutine(DisplayText(statusScale, statusAnim));
    }
    public void Begin()
    {
        canPlay = true;
        Levels[levelSelect].GetComponentInChildren<GameGestion>().Reset();
        bestCanvas.alpha = 0;
        StartCoroutine(HideText(statusScale, statusAnim));
        currentPos = closedPos;
        currentTime = 0;
        open = false;
        begin.text = "Restart Level";
        buttonCanvas.gameObject.SetActive(true);
        StartCoroutine(Fade(false, buttonCanvas));
        StartCoroutine(Fade(false, scoreCanvas));
        StartCoroutine(Fade(false, timerCanvas));
    }
    public void Quit()
    {
        canPlay = false;

        Application.Quit();
    }
    public void ToggleSound()
    {
        canPlay = false;

        if (audioOn.enabled)
        {
            audioOn.enabled = false;
            sound.text = "Audio On";
        }
        else
        {
            audioOn.enabled = true;
            sound.text = "Audio Off";
        }
    }
    public void OpenMenu()
    {

        if (open)
        {
            canPlay = true;
            currentPos = closedPos;
            currentTime = 0;
            open = false;
        }
        else
        {
            canPlay = false;
            currentPos = openPos;
            currentTime = 0;
            open = true;
        }
    }
    IEnumerator Fade(bool fadeOut, CanvasGroup canvas)
    {
        float basis = 0;
        float end = 1;
        if (fadeOut)
        {
            basis = 1;
            end = 0;
        }
        float current = 0;
        while (current < 1)
        {
            current += Time.deltaTime;
            canvas.alpha = Mathf.Lerp(basis, end, current);
            yield return null;
        }
        canvas.alpha = end;
        if(canvas == aboutCanvas)
        {
            if (about)
            {
                about = false;
                aboutCanvas.transform.GetChild(0).gameObject.SetActive(false);
                aboutActive = false;
            }
            else
            {
                about = true;
                aboutActive = false;
            }
        }
        yield break;
    }
    IEnumerator HideText(Transform scale, TextAnim anim)
    {
        anim.active = false;
        float current = 0;
        while (current < 0.2f)
        {
            current += Time.deltaTime;
            scale.localScale = new Vector2(Mathf.Lerp(statusScale.localScale.x, 0, current * 5), 1);
            yield return null;
        }
        scale.localScale = new Vector2(0, 1);
        anim.active = false;
        yield break;
    }
    IEnumerator DisplayLevels(bool OffScreen)
    {
        float basis = -500;
        float end = -32.7F;
        if (OffScreen)
        {
            basis = -32.7F;
            end = -500;
        }
        float current = 0;
        while (current < 0.2f)
        {
            current += Time.deltaTime;
            levelsGrid.parent.transform.localPosition = new Vector2(-1.7f, Mathf.Lerp(basis, end, current * 5));
            yield return null;
        }
        levelsGrid.parent.transform.localPosition = new Vector2(-1.7f, end);
        if (OffScreen)
        {
            for (int i = 0; i < levelsGrid.childCount; i++)
            {
                GameObject.Destroy(levelsGrid.GetChild(i).gameObject);
            }
        }
        yield break;
    }
    IEnumerator DisplayText(Transform scale, TextAnim anim)
    {
        anim.active = true;
        float current = 0;
        while (current < 0.2f)
        {
            current += Time.deltaTime;
            scale.localScale = new Vector2(Mathf.Lerp(0, 2, current * 5), 1);
            yield return null;
        }
        current = 0;
        while (current < 0.2f)
        {
            current += Time.deltaTime;
            scale.localScale = new Vector2(Mathf.Lerp(2, 0.8f, current * 5), 1);
            yield return null;
        }
        current = 0;
        while (current < 0.1f)
        {
            current += Time.deltaTime;
            scale.localScale = new Vector2(Mathf.Lerp(0.8f, 1, current * 10), 1);
            yield return null;
            scale.localScale = new Vector2(1, 1);
        }
        yield break;
    }
}
