﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAnim : MonoBehaviour
{
    Transform tr;
    public bool active = true;
    bool plusScale = true;
    bool plusEuler = true;
    float currentTime;
    float amountScale = 0.05f;
    float amountEuler = 1f;
    void Start()
    {
        tr = this.transform;
    }
    void Update()
    {
        if (active)
        {
            currentTime += Time.deltaTime;
            if (tr.localScale.x < 1)
            {
                plusScale = true;
            }
            if (tr.localScale.x > 1.2)
            {
                plusScale = false;
            }
            if (currentTime > 3)
            {
                if (plusEuler)
                {
                    plusEuler = false;
                }
                else
                {
                    plusEuler = true;
                }
                currentTime = 0;
            }
            if (plusEuler)
            {
                tr.localEulerAngles = new Vector3(0, 0, tr.localEulerAngles.z + Time.deltaTime * amountEuler);
            }
            else
            {
                tr.localEulerAngles = new Vector3(0, 0, tr.localEulerAngles.z - Time.deltaTime * amountEuler);
            }
            if (plusScale)
            {
                tr.localScale = new Vector2(tr.localScale.x + Time.deltaTime * amountScale, tr.localScale.y + Time.deltaTime * amountScale);
            }
            else
            {
                tr.localScale = new Vector2(tr.localScale.x - Time.deltaTime * amountScale, tr.localScale.y - Time.deltaTime * amountScale);
            }
        }
    }
}
