﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cat : MonoBehaviour
{

        public static Dictionary<int, Sprite> catSprites;
        private int number = 17;
        [SerializeField] private float scoreMultiplier = 500;
        public float animMulitplier = 1;
        private Animator animator;
        public int score;
        private SpriteRenderer spriteRenderer;

        private void Start()
        {
            Sprite sprite;
            spriteRenderer = GetComponent<SpriteRenderer>();
            animator = GetComponent<Animator>();
            catSprites = new Dictionary<int, Sprite>();
            for(int i = 0; i < 17 ; i++)
            {
                sprite = Resources.Load<Sprite>("Sprites/Cat/cat" + i.ToString());
                if(!sprite)
                    Debug.LogError("ScoreManager LOADING ERROR: resource \"Resources/Sprites/Cat/cat" + i.ToString() + "\" not found");
                catSprites.Add(i, sprite);
            }
        }

        private void Update(){

            score = ScoreManager.Score;

            animMulitplier = score / scoreMultiplier;
            spriteRenderer.sprite = catSprites[(int)animMulitplier];
            animator.SetFloat("Multiplier", animMulitplier);

        }
}
